package com.redtoorange.game.events;

import com.badlogic.gdx.physics.box2d.Contact;

public interface EndContactListener {
	public void endContact( Contact contact );
}
