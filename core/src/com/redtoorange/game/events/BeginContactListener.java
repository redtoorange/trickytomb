package com.redtoorange.game.events;

import com.badlogic.gdx.physics.box2d.Contact;

public interface BeginContactListener {
	void beginContact( Contact contact );
}
