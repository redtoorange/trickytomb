package com.redtoorange.game.events;

import com.badlogic.gdx.physics.box2d.Contact;
import com.badlogic.gdx.physics.box2d.ContactImpulse;
import com.badlogic.gdx.physics.box2d.ContactListener;
import com.badlogic.gdx.physics.box2d.Manifold;
import com.badlogic.gdx.utils.Array;

public class ContactEventController implements ContactListener{
	private Array<BeginContactListener> beginContactListeners = new Array<BeginContactListener>(  );
	private Array<EndContactListener> endContactListeners = new Array<EndContactListener>(  );

	public void addBeginContactEventListener( BeginContactListener bcl){
		beginContactListeners.add( bcl );
	}

	public void removeBeginContactEventListener( BeginContactListener bcl ){
		beginContactListeners.removeValue( bcl, true );
	}

	public void addEndContactEventListener( EndContactListener ecl){
		endContactListeners.add( ecl );
	}

	public void removeEndContactEventListener( EndContactListener ecl ){
		endContactListeners.removeValue( ecl, true );
	}


	@Override
	public void beginContact( Contact contact ) {
		for ( BeginContactListener bcl : beginContactListeners )
			bcl.beginContact( contact );
	}


	@Override
	public void endContact( Contact contact ) {
		for ( EndContactListener ecl : endContactListeners )
			ecl.endContact( contact );
	}

	@Override
	public void preSolve( Contact contact, Manifold oldManifold ) { }
	@Override
	public void postSolve( Contact contact, ContactImpulse impulse ) { }
}
