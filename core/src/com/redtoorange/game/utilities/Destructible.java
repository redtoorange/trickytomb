package com.redtoorange.game.utilities;

public interface Destructible {
	void destroy();
}
