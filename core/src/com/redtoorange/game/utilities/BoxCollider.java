package com.redtoorange.game.utilities;

import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.BodyDef;
import com.badlogic.gdx.physics.box2d.PolygonShape;
import com.badlogic.gdx.physics.box2d.World;

public class BoxCollider {
	protected Body body;

	public BoxCollider( Vector2 centerPosition, float width, float height, World world){
		BodyDef bDef = new BodyDef();
		bDef.type = BodyDef.BodyType.StaticBody;
		bDef.position.set( centerPosition );
		body = world.createBody( bDef );

		PolygonShape shape = new PolygonShape();
		shape.setAsBox( width, height );

		body.createFixture( shape, 1 );

		shape.dispose();
	}

	public void setUserData( Object o ){
		body.setUserData( o );
	}

	public void setType( BodyDef.BodyType type){
		body.setType( type );
	}

	public void setDensity( float density ){
		body.getFixtureList().get( 0 ).setDensity( density );
	}


	public Object getUserData(){
		return body.getUserData();
	}

	public Vector2 getPosition(){
		return body.getPosition();
	}

	public void destroy(){
		body.getWorld().destroyBody( body );
	}

	public void enableCollisions(){
		body.setActive( true );
	}

	public void disableCollisions(){
		body.setActive( false );
	}
}
