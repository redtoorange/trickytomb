package com.redtoorange.game.utilities;

public interface Disableable {
	void disable();
	void enable();
}
