package com.redtoorange.game.utilities;

import com.badlogic.gdx.utils.Array;

public class Destroyer {
	public static Destroyer S = new Destroyer();
	Array<Destructible> readyToBeDestroyed;

	public Destroyer(){
		readyToBeDestroyed = new Array<Destructible>(  );
	}

	public void addObjectToQueue( Destructible d ){
		readyToBeDestroyed.add( d );
	}

	public void cull(){
		for(Destructible d : readyToBeDestroyed ){
			d.destroy();
		}
		readyToBeDestroyed.clear();
	}
}
