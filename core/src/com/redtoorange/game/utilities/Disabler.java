package com.redtoorange.game.utilities;

import com.badlogic.gdx.utils.Array;

public class Disabler {
	public static Disabler S = new Disabler();
	Array<Disableable> toBeDisabled;
	Array<Disableable> toBeEnabled;

	public Disabler(){
		toBeDisabled = new Array<Disableable>(  );
		toBeEnabled = new Array<Disableable>(  );
	}

	public void disableObject(Disableable d){
		toBeDisabled.add( d );
	}

	public void enableObject(Disableable d){
		toBeEnabled.add( d );
	}


	public void cull(){
		for(Disableable d : toBeDisabled ){
			d.disable();
		}
		toBeDisabled.clear();

		for(Disableable d : toBeEnabled){
			d.enable();
		}
		toBeEnabled.clear();
	}
}
