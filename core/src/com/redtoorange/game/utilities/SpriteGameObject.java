package com.redtoorange.game.utilities;

import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;

public class SpriteGameObject extends GameObject{
	private Sprite sprite;

	public SpriteGameObject( GameObject parent, String name, Sprite sprite ) {
		super( parent, name );
		this.sprite = sprite;
	}


	@Override
	public void draw( SpriteBatch batch ) {
		super.draw( batch );
		sprite.draw( batch );
	}
}
