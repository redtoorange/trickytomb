package com.redtoorange.game.utilities;

import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.BodyDef;
import com.badlogic.gdx.physics.box2d.FixtureDef;
import com.badlogic.gdx.physics.box2d.PolygonShape;
import com.badlogic.gdx.physics.box2d.Shape;

public class Box2DHelper {
	public static BodyDef createBodyDef( Vector2 position, BodyDef.BodyType type){
		BodyDef bDef = new BodyDef();
		bDef.type = type;
		bDef.position.set( position );

		return bDef;
	}

	public static FixtureDef createFixtureDef( Shape shape, float density){
		FixtureDef fDef = new FixtureDef();
		fDef.shape = shape;
		fDef.density = density;
		return fDef;
	}

	public static FixtureDef createFixtureDef( Shape shape, float density, float friction, float restitution){
		FixtureDef fDef = createFixtureDef( shape, density );
		fDef.friction = friction;
		fDef.restitution = restitution;
		return fDef;
	}

	public static PolygonShape createPolyShape( float width, float height){
		PolygonShape poly = new PolygonShape();
		poly.setAsBox( width, height );
		return poly;
	}
}


