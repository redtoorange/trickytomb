package com.redtoorange.game.utilities;

import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.utils.Array;

public class GameObject {
    protected GameObject parent;
    protected Array<GameObject> children;
    protected String name;

	protected float zLevel = 0f;

    public GameObject( GameObject parent, String name ){
        children = new Array<GameObject>(  );
        this.parent = parent;
        this.name = name;
    }

    public void update(float delta){
        for ( GameObject c : children )
            c.update( delta );
    }

    public void draw( SpriteBatch batch){
        for ( GameObject c : children )
            c.draw( batch );
    }

    public void addChild( GameObject c ){
        children.add( c );
    }

    public void removeChild( GameObject c ){
        children.removeValue( c, true );
    }
    public String getName(){
        return name;
    }

    public String printTree( int level){
		String s = "";

		for(int i = 0; i < level; i++){
			s += "-";
		}
		s += name + "\n";

		if(children.size > 0){
			level++;
			for(GameObject c : children){
				for(int i = 0; i < level; i++){
					s += "-";
				}
				s += c.printTree( level );
			}
		}
		return s;
	}

	public float getZLevel(){
		return zLevel;
	}

	public float compareTo( GameObject go ){
		return zLevel - go.getZLevel();
	}
}
