package com.redtoorange.game.utilities;

public class Constants {
    public static final String DUNGEON_PATH = "dungeons/";
    public static final String TITLE = "Tricky Tomb";
    public static final int WINDOW_WIDTH = 800;
    public static final int WINDOW_HEIGHT = 600;

    public static final int WORLD_WIDTH = 10;
    public static final int WORLD_HEIGHT = 9;
}
