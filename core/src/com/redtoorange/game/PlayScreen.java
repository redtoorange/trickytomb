package com.redtoorange.game;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.ScreenAdapter;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.FPSLogger;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Box2DDebugRenderer;
import com.badlogic.gdx.physics.box2d.World;
import com.badlogic.gdx.utils.Scaling;
import com.badlogic.gdx.utils.viewport.ScalingViewport;
import com.badlogic.gdx.utils.viewport.Viewport;
import com.redtoorange.game.events.ContactEventController;
import com.redtoorange.game.map.MapController;
import com.redtoorange.game.utilities.Constants;
import com.redtoorange.game.utilities.Destroyer;
import com.redtoorange.game.utilities.Disabler;
import com.redtoorange.game.utilities.GameObject;

import static com.badlogic.gdx.Gdx.gl;

public class PlayScreen extends ScreenAdapter {
	private OrthographicCamera camera;
	private Viewport viewport;
	private SpriteBatch batch;
	private GameObject sceneRoot;
	private MapController testMap;
	private World world;
	private Box2DDebugRenderer box2DDebugRenderer;
	private boolean debugging = false;
	private ContactEventController contactEventController;
	private Player player;
	private FPSLogger logger;

	public PlayScreen( ) {
		super( );
		logger = new FPSLogger();
		contactEventController = new ContactEventController( );

		world = new World( new Vector2( 0, 0 ), true );
		box2DDebugRenderer = new Box2DDebugRenderer( );

		sceneRoot = new GameObject( null, "SceneRoot" );
		batch = new SpriteBatch( );

		//height and width given in arbitrary units
		camera = new OrthographicCamera( Constants.WORLD_WIDTH, Constants.WORLD_HEIGHT );
		viewport = new ScalingViewport( Scaling.fit, Constants.WORLD_WIDTH, Constants.WORLD_HEIGHT, camera );

		player = new Player( sceneRoot, "Player", testMap, world );
		testMap = new MapController( sceneRoot, "Dungeon01.tmx", camera, world, 1, 16, contactEventController, player);


		world.setContactListener( contactEventController );


		sceneRoot.addChild( testMap );
		sceneRoot.addChild( player );

		System.out.println( sceneRoot.printTree( 0 ) );
	}

	@Override
	public void resize( int width, int height ) {
		viewport.update( width, height );
		camera.update( );
	}

	@Override
	public void show( ) {
		super.show( );
	}

	@Override
	public void render( float delta ) {
		update( delta );
		draw( );
	}

	private void draw( ) {
		camera.update( );
		clearScreen( );


		batch.setProjectionMatrix( camera.combined );
		batch.begin( );

		sceneRoot.draw( batch );

		batch.end( );

		testMap.drawAbovePlayer( batch );

		if ( debugging )
			box2DDebugRenderer.render( world, camera.combined );
	}

	private void clearScreen( ) {
		gl.glClearColor( Color.BLACK.r, Color.BLACK.g, Color.BLACK.b, Color.BLACK.a );
		gl.glClear( GL20.GL_COLOR_BUFFER_BIT | GL20.GL_DEPTH_BUFFER_BIT );
	}

	private void update( float delta ) {
		sceneRoot.update( delta );

		camera.position.set( player.getCenter(), 0 );
		world.step( delta, 6, 2 );

		Destroyer.S.cull();
		Disabler.S.cull();
		testMap.changeMapPiece();

		if( Gdx.input.isKeyJustPressed( Input.Keys.P ))
			debugging = !debugging;
	}

	@Override
	public void dispose( ) {
		super.dispose( );
	}
}
