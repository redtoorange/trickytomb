package com.redtoorange.game;

import com.badlogic.gdx.Game;

public class MainGame extends Game {

	public void create () {
		setScreen( new PlayScreen() );
	}
}
