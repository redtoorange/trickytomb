package com.redtoorange.game.map;

import com.badlogic.gdx.maps.MapProperties;
import com.badlogic.gdx.maps.objects.PolygonMapObject;
import com.badlogic.gdx.maps.objects.RectangleMapObject;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.*;
import com.redtoorange.game.Player;
import com.redtoorange.game.events.BeginContactListener;
import com.redtoorange.game.events.EndContactListener;
import com.redtoorange.game.utilities.Box2DHelper;

import java.util.Scanner;

public class Key implements BeginContactListener, EndContactListener {
	private RectangleMapObject rectangleMapObject;
	private Body body;
	private DungeonMap map;
	private boolean triggered = false;
	private int contractors = 0;

	public Key( RectangleMapObject rectangleMapObject, Rectangle rec, World world, DungeonMap map ) {
		this.map = map;
		this.rectangleMapObject = rectangleMapObject;

		buildBody( rec, world );
		map.contactEventController.addBeginContactEventListener( this );
		map.contactEventController.addEndContactEventListener( this );
	}

	private void buildBody( Rectangle rec, World world ) {
		body = world.createBody( Box2DHelper.createBodyDef(
				new Vector2( rec.getX( ) + rec.getWidth( ) / 2f, rec.getY( ) + rec.getHeight( ) / 2f  ),
				BodyDef.BodyType.KinematicBody
		) );

		PolygonShape shape = Box2DHelper.createPolyShape( rec.getWidth( ) / 2f, rec.getHeight( ) / 2f );
		FixtureDef fDef = Box2DHelper.createFixtureDef( shape, 1 );

		fDef.isSensor = true;
		body.createFixture( fDef );

		shape.dispose( );
	}

	public void open( ) {
		if(!triggered) {
			triggered = true;
			for ( Wall w : map.getWalls( ) )
				if( w.getName() != null && w.getName( ).equalsIgnoreCase( (String) getMapProperties().get( "WallToOpen" ) ) )
					w.disableWall( );


			if( getMapProperties().get( "RevealDoor", String.class ).equals( "True" ) ) {
				Scanner scanner = new Scanner( (String) getMapProperties().get( "LayerName" ) );
				scanner.useDelimiter( ";" );

				while ( scanner.hasNext( ) ) map.revealLayer( scanner.next( ) );
			}
		}
	}


	public void close( ) {
		if(triggered) {
			triggered = false;
			for ( Wall w : map.getWalls( ) )
				if( w.getName() != null && w.getName( ).equalsIgnoreCase( (String) getMapProperties().get( "WallToOpen" ) ) )
					w.enableWall( );


			if( getMapProperties().get( "RevealDoor", String.class ).equals( "True" ) ) {
				Scanner scanner = new Scanner( (String) getMapProperties().get( "LayerName" ) );
				scanner.useDelimiter( ";" );

				while ( scanner.hasNext( ) )
					map.hideLayer( scanner.next( ) );
			}
		}
	}

	@Override
	public void beginContact( Contact contact ) {
		if ( 	( contact.getFixtureA( ).getBody( ).equals( body ) && (
						contact.getFixtureB( ).getBody( ).getUserData( ) instanceof Player ||
						contact.getFixtureB( ).getBody( ).getUserData( ) instanceof MapGameObject)
				) ||
				( contact.getFixtureB( ).getBody( ).equals( body ) && (
						contact.getFixtureA( ).getBody( ).getUserData( ) instanceof Player ||
						contact.getFixtureA( ).getBody( ).getUserData( ) instanceof MapGameObject) ) )
			contractors++;
		if(contractors > 0){
			open();
		}
	}

	@Override
	public void endContact( Contact contact ) {
//		if ( 	( contact.getFixtureA( ).getBody( ).equals( body ) && (contact.getFixtureB( ).getBody( ).getUserData( ) instanceof Player ||
//				contact.getFixtureB( ).getBody( ).getUserData( ) instanceof MapGameObject) ) ||
//				( contact.getFixtureB( ).getBody( ).equals( body ) && (contact.getFixtureA( ).getBody( ).getUserData( ) instanceof Player ||
//						contact.getFixtureA( ).getBody( ).getUserData( ) instanceof MapGameObject) ) )
//			contractors--;
//		if(contractors<= 0){
//			close();
//		}


	}

	public void setActive( boolean active ){
		body.setActive( active );
	}

	public MapProperties getMapProperties(){
		return rectangleMapObject.getProperties();
	}
}
