package com.redtoorange.game.map;

import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.World;
import com.redtoorange.game.Player;
import com.redtoorange.game.events.ContactEventController;
import com.redtoorange.game.utilities.GameObject;

public class MapController extends GameObject {
	public ContactEventController contactEventController;
	private DungeonMap currentDungeonMap;
	private Player player;
	private boolean queuedMapChange = false;
	private String newMapPiece;
	private Vector2 playPos;

    public MapController( GameObject parent, String mapPath, OrthographicCamera camera, World world, float unitsPerTile, float tileWidth, ContactEventController contactEventController, Player player ){
		super( parent, "MapController" );
		this.contactEventController = contactEventController;
		this.player = player;

		currentDungeonMap = new DungeonMap( this, mapPath, camera, world,
				unitsPerTile, tileWidth, contactEventController, this );
		children.add( currentDungeonMap );

		DungeonMap nextMap = new DungeonMap( this, "Dungeon02.tmx", camera, world, unitsPerTile,
				tileWidth, contactEventController, this);
		nextMap.setActive( false );
		children.add( nextMap );

		nextMap = new DungeonMap( this, "Dungeon03.tmx", camera, world, unitsPerTile,
				tileWidth, contactEventController, this);
		nextMap.setActive( false );
		children.add( nextMap );

		nextMap = new DungeonMap( this, "Dungeon04.tmx", camera, world, unitsPerTile,
				tileWidth, contactEventController, this);
		nextMap.setActive( false );
		children.add( nextMap );
    }

	@Override
	public void update( float delta ) {
		currentDungeonMap.update( delta );
	}

	@Override
	public void draw( SpriteBatch batch ) {
		currentDungeonMap.draw( batch );
	}

	public void drawAbovePlayer( SpriteBatch batch ) {
		currentDungeonMap.drawAbovePlayer( batch );
	}

	public void queueMapChange( String newMapPiece, Vector2 playPos){
		this.newMapPiece = newMapPiece;
		this.playPos = playPos;
		queuedMapChange = true;
	}

	public void changeMapPiece( ){
		if(queuedMapChange){
			queuedMapChange = false;

			System.out.println( "Teleporting to: " + newMapPiece +
					"\t" + playPos  );

			if(player != null)
				player.setPosition( playPos );
			currentDungeonMap.setActive( false );

			for(GameObject go : children)
				if(go.getName().equals( newMapPiece ))
					currentDungeonMap = (DungeonMap) go;

			currentDungeonMap.setActive( true );
		}
	}
}
