package com.redtoorange.game.map;

import com.badlogic.gdx.maps.MapProperties;
import com.badlogic.gdx.maps.objects.RectangleMapObject;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.World;
import com.redtoorange.game.utilities.BoxCollider;
import com.redtoorange.game.utilities.Disableable;
import com.redtoorange.game.utilities.Disabler;

public class Wall implements Disableable {
	private BoxCollider collider;
	private RectangleMapObject rectangleMapObject;
	private DungeonMap map;

	public Wall( Rectangle rec, World world, RectangleMapObject rectangleMapObject, DungeonMap map){
		this.map = map;
		this.rectangleMapObject = rectangleMapObject;

		buildBody( rec, world );
	}

	private void buildBody( Rectangle rec, World world ) {
		Vector2 pos = new Vector2( rec.getX() + rec.getWidth() / 2f, rec.getY() + rec.getHeight() / 2f );
		float width = rec.getWidth() / 2f;
		float height = rec.getHeight() / 2f;
		collider = new BoxCollider( pos,  width, height, world );

		collider.setUserData( this );
	}

	public String getName(){
		return rectangleMapObject.getName();
	}

	public void destroy(){
		map.getWalls().removeValue( this, true );
		collider.destroy();
	}

	boolean enabled = true;
	boolean active;

	public void setActive( boolean active ){
		this.active = active;

		if(active && enabled)
			collider.enableCollisions();
		else
			collider.disableCollisions();
	}

	public MapProperties getMapProperties(){
		return rectangleMapObject.getProperties();
	}

	public void disableWall(){
		enabled = false;
		Disabler.S.disableObject( this );
	}


	public void disable(){
		collider.disableCollisions();
	}


	public void enableWall(){
		enabled = true;
		Disabler.S.enableObject( this );
	}

	public void enable(){
		collider.enableCollisions();
	}
}
