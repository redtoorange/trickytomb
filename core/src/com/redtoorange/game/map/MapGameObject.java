package com.redtoorange.game.map;

import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.physics.box2d.Body;
import com.redtoorange.game.utilities.GameObject;

public class MapGameObject extends GameObject{
	private Body body;
	private Sprite sprite;
	private float width;
	private float height;

	public MapGameObject( GameObject parent, String name, Body body, Sprite sprite, float width, float height){
		super( parent, name );
		this.body = body;
		this.sprite = sprite;
		this.width = width;
		this.height = height;

		body.setLinearDamping( 10 );
		body.getFixtureList().get( 0 ).setDensity( 10 );

		sprite.setSize( width, height );
		System.out.println( body.getPosition() );
		sprite.setPosition( body.getPosition().x, body.getPosition().y - sprite.getHeight());
		System.out.println( sprite.getX() + ", " + sprite.getY());
		body.setUserData( this );
	}

	@Override
	public void update( float delta ) {
		sprite.setPosition( body.getPosition().x, body.getPosition().y - sprite.getHeight());
	}

	@Override
	public void draw( SpriteBatch batch ) {
		sprite.draw( batch );
	}

	public void setActive( boolean active ){
		body.setActive( active );
	}
}
