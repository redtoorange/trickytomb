package com.redtoorange.game.map;

import com.badlogic.gdx.maps.MapProperties;
import com.badlogic.gdx.maps.objects.RectangleMapObject;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.*;
import com.redtoorange.game.events.BeginContactListener;
import com.redtoorange.game.utilities.Box2DHelper;

import java.util.Scanner;

public class Teleport implements BeginContactListener {
	private Body body;
	private RectangleMapObject rectangleMapObject;
	private DungeonMap map;

	private String destinationMap;
	private Vector2 destininationPosition;

	private boolean activated = false;

	public Teleport( Rectangle rec, World world, RectangleMapObject rectangleMapObject, DungeonMap map ) {
		this.map = map;
		this.rectangleMapObject = rectangleMapObject;

		buildBody( rec, world );
		map.contactEventController.addBeginContactEventListener( this );

		Scanner scan = new Scanner( rectangleMapObject.getProperties( ).get( "Destination" ).toString( ) );
		scan.useDelimiter( "," );
		destinationMap = scan.next( );
		destininationPosition = new Vector2( scan.nextInt( ), scan.nextInt( ) );
	}

	private void buildBody( Rectangle rec, World world ) {
		body = world.createBody( Box2DHelper.createBodyDef(
				new Vector2( rec.getX( ) + rec.getWidth( ) / 2f, rec.getY( ) + rec.getHeight( ) / 2f ),
				BodyDef.BodyType.KinematicBody
		) );

		PolygonShape shape = Box2DHelper.createPolyShape( rec.getWidth( ) / 2f, rec.getHeight( ) / 2f  );
		FixtureDef fDef = Box2DHelper.createFixtureDef( shape, 1 );
		fDef.isSensor = true;

		body.createFixture( fDef );
		shape.dispose( );
	}

	@Override
	public void beginContact( Contact contact ) {
		Body bodyA = contact.getFixtureA( ).getBody( );
		Body bodyB = contact.getFixtureB( ).getBody( );

		if( bodyA.equals( body ) || bodyB.equals( body ) )
			teleportToDestination( );
	}

	public void teleportToDestination() {
		map.getMapController( ).queueMapChange( destinationMap, destininationPosition );
	}

	public void recievedTeleport() {
	}

	public void setActive( boolean active ) {
		body.setActive( active );
	}

	public MapProperties getMapProperties(){
		return rectangleMapObject.getProperties();
	}
}
