package com.redtoorange.game.map;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.assets.loaders.FileHandleResolver;
import com.badlogic.gdx.assets.loaders.resolvers.InternalFileHandleResolver;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.maps.MapLayer;
import com.badlogic.gdx.maps.MapObjects;
import com.badlogic.gdx.maps.MapProperties;
import com.badlogic.gdx.maps.objects.PolygonMapObject;
import com.badlogic.gdx.maps.objects.RectangleMapObject;
import com.badlogic.gdx.maps.tiled.TiledMap;
import com.badlogic.gdx.maps.tiled.TiledMapTileLayer;
import com.badlogic.gdx.maps.tiled.TmxMapLoader;
import com.badlogic.gdx.maps.tiled.renderers.OrthogonalTiledMapRenderer;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.physics.box2d.*;
import com.badlogic.gdx.utils.Array;
import com.redtoorange.game.events.ContactEventController;
import com.redtoorange.game.utilities.Constants;
import com.redtoorange.game.utilities.GameObject;
import com.redtoorange.game.utilities.SpriteGameObject;

import java.util.Scanner;

public class DungeonMap extends GameObject{
	private boolean active = true;

	private float scale;
	private OrthogonalTiledMapRenderer mapRenderer;
	private OrthographicCamera camera;
	private TiledMap map;
	public ContactEventController contactEventController;
	public Array<Wall> walls;
	public Array<Key> keys;
	public Array<Teleport> teleports;
	public Array<MapGameObject> mapGameObjects;
	private World world;
	private MapController mapController;
	public String name;

	public DungeonMap( GameObject parent, String mapPath, OrthographicCamera camera, World world, float unitsPerTile, float tileWidth, ContactEventController contactEventController, MapController mapController ) {
		super( parent, mapPath );
		this.mapController = mapController;
		this.contactEventController = contactEventController;
		this.world = world;


		//70 units on the map equal 1 potato unit
		scale = unitsPerTile / tileWidth;
		FileHandleResolver resolver = new InternalFileHandleResolver( );
		TmxMapLoader mapLoader = new TmxMapLoader( resolver );
		map = mapLoader.load( Constants.DUNGEON_PATH + mapPath );

		mapRenderer = new OrthogonalTiledMapRenderer( map, scale );
		this.camera = camera;
		name = mapPath;

		populateCollisionItems( );
		populateKeys( );
		populateTeleports( );

		testMapObjects();
		buildTileMatrix();
	}

	private void testMapObjects(){
		mapGameObjects = new Array<MapGameObject>(  );

		if(map.getLayers().get( "MapObjects" ) != null){
			MapObjects objects = map.getLayers().get( "MapObjects" ).getObjects();

			for( PolygonMapObject poly : objects.getByType( PolygonMapObject.class )){
				float[] verts =  poly.getPolygon().getVertices();
				for(int i = 0; i < verts.length; i++)
					verts[i] = verts[i] * scale;

				float x = poly.getPolygon().getX() * scale;
				float y = poly.getPolygon().getY() * scale;

				MapProperties properties = poly.getProperties();

				PolygonShape shape = new PolygonShape();
				shape.set( verts );

				BodyDef bd = new BodyDef();
				bd.type = BodyDef.BodyType.DynamicBody;
				bd.position.set( x, y );

				Body b = world.createBody( bd );
				b.createFixture( shape, .1f );
				b.setFixedRotation( true );


				Scanner propertyScanner = new Scanner( (String)properties.get( "Density" ) );
				FixtureDef fDef = new FixtureDef();
				float density = propertyScanner.nextFloat();
				fDef.density = density;

				if(properties.containsKey( "LinearFriction" )) {
					propertyScanner = new Scanner( ( String ) properties.get( "LinearFriction" ) );
					float linearFriction = propertyScanner.nextFloat( );
					fDef.friction = linearFriction;
				}
				else
					fDef.friction = 0;

				fDef.shape = shape;

				b.createFixture( fDef );

				if(properties.containsKey( "Sprite" )) {
					Sprite s = new Sprite( new Texture( Gdx.files.internal( ( String ) properties.get( "Sprite" ) ) ), 0, 0, 16, 16 );
					children.add( new MapGameObject( this, "PushableTile", b, s, 1, 1 ) );
				}
			}
		}

	}

	private Rectangle rectangleMapObjectToRectangle( RectangleMapObject r ) {
		Rectangle rect = new Rectangle( r.getRectangle( ) );
		rect.setPosition( rect.x * scale, rect.y * scale );
		rect.setSize( rect.width * scale, rect.height * scale );
		return rect;
	}

	private void populateCollisionItems() {
		walls = new Array<Wall>( );
		MapLayer layer = map.getLayers( ).get( "Collisions" );

		for ( RectangleMapObject r : layer.getObjects( ).getByType( RectangleMapObject.class ) )
			walls.add( new Wall( rectangleMapObjectToRectangle( r ), world, r, this ) );
	}

	private void populateKeys() {
		keys = new Array<Key>( );
		MapLayer layer = map.getLayers( ).get( "Keys" );

		if( layer != null ) {
			for ( RectangleMapObject r : layer.getObjects( ).getByType( RectangleMapObject.class ) )
				keys.add( new Key( r, rectangleMapObjectToRectangle( r ), world, this ) );
		}
	}

	private void populateTeleports() {
		teleports = new Array<Teleport>( );
		MapLayer layer = map.getLayers( ).get( "Teleports" );

		for ( RectangleMapObject r : layer.getObjects( ).getByType( RectangleMapObject.class ) )
			teleports.add( new Teleport( rectangleMapObjectToRectangle( r ), world, r, this ) );
	}


	public void update( float delta ) {
		for(MapGameObject o : mapGameObjects ){
			o.update( delta );
		}
	}


	public void draw( SpriteBatch batch ) {
		if(active)
			super.draw( batch );

//		Vector2 cameraPosition = new Vector2( camera.position.x, camera.position.y );
//		float cameraWidth = camera.viewportWidth;
//		float cameraHeight = camera.viewportHeight;
//
//		Rectangle viewRect = new Rectangle( cameraPosition.x - cameraWidth/1.5f, cameraPosition.y - cameraHeight/1.5f, cameraWidth * 1.2f, cameraHeight * 1.2f  );
	}

	private void buildTileMatrix(){
		Array<TiledMapTileLayer> layers = map.getLayers().getByType( TiledMapTileLayer.class );

		for(TiledMapTileLayer layer : layers) {
			int width = layer.getWidth( );
			int height = layer.getHeight( );

			float tileWidth = layer.getTileWidth( );
			float tileHeight = layer.getTileHeight( );

			Sprite sprite = new Sprite( );
			sprite.setSize( 1, 1 );
			TextureRegion t;
			for ( int x = 0; x < width; x++ ) {
				for ( int y = 0; y < height; y++ ) {
					if( layer.getCell( x, y ) != null ) {
						t = layer.getCell( x, y ).getTile( ).getTextureRegion( );
						if( t != null ) {
							sprite.setRegion( t );
							sprite.setX( x * tileWidth * scale );
							sprite.setY( y * tileHeight * scale );
							children.add( new SpriteGameObject( this, (x + ", "+ y), new Sprite( sprite ) ) );
						}
					}
				}
			}
			Array<GameObject> list = new Array<GameObject>(  );
		}
	}

	public void drawAbovePlayer( SpriteBatch batch ) {
		TiledMapTileLayer layer = (TiledMapTileLayer) map.getLayers( ).get( "AbovePlayer" );
		if( layer != null && layer.isVisible( ) ) {
			mapRenderer.setView( camera );
			mapRenderer.getBatch( ).setProjectionMatrix( camera.combined );
			mapRenderer.getBatch( ).begin( );
			mapRenderer.renderTileLayer( layer );
			mapRenderer.getBatch( ).end( );
		}
	}

	public void revealLayer( String layerName ) {
		System.out.println( layerName + " enabled." );
		map.getLayers( ).get( layerName ).setVisible( true );
	}

	public void hideLayer( String layerName ) {
		map.getLayers( ).get( layerName ).setVisible( false );
	}

	public Array<Wall> getWalls() {
		return walls;
	}

	public Array<Key> getKeys() {
		return keys;
	}

	public Array<Teleport> getTeleports() {
		return teleports;
	}

	public boolean isActive() {
		return active;
	}

	public void setActive( boolean active ) {
		for ( Wall w : walls )
			w.setActive( active );

		for ( Key k : keys )
			k.setActive( active );

		for ( Teleport t : teleports )
			t.setActive( active );

		for( MapGameObject o : mapGameObjects )
			o.setActive( active );

		this.active = active;
	}

	public MapController getMapController() {
		return mapController;
	}
}
