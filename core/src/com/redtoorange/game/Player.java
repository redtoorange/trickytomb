package com.redtoorange.game;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.*;
import com.badlogic.gdx.utils.Array;
import com.redtoorange.game.map.MapController;
import com.redtoorange.game.utilities.Constants;
import com.redtoorange.game.utilities.GameObject;

public class Player extends GameObject {
	private Sprite sprite;
	private Body body;
	private Vector2 inputVelocity;
	private float speed = 7.5f;
	private MapController currentMap;
	private Animation upAnim;
	private Animation downAnim;
	private Animation leftAnim;
	private Animation rightAnim;
	private Facing currentFacing = Facing.RIGHT;
	private Texture playerTexture;
	private float currentAnimationFrame = 0.0f;
	private float animationRate = 1/10f;

	public Player( GameObject parent, String name, MapController map, World world ) {
		super( parent, name );
		currentMap = map;
		inputVelocity = Vector2.Zero;

		setupSprite( );
		setupAnimation();
		buildBody( world );
	}

	private void setupAnimation(){
		Array<TextureRegion> keyFrames = new Array<TextureRegion>(  );

		keyFrames.add( new TextureRegion( playerTexture, 0, 0, 16, 16 ) );
		keyFrames.add( new TextureRegion( playerTexture, 0, 16, 16, 16 ) );
		downAnim = new Animation( animationRate, keyFrames, Animation.PlayMode.LOOP );
		keyFrames.clear();

		keyFrames.add( new TextureRegion( playerTexture, 16, 0, 16, 16 ) );
		keyFrames.add( new TextureRegion( playerTexture, 16, 16, 16, 16 ) );
		leftAnim = new Animation( animationRate, keyFrames, Animation.PlayMode.LOOP );
		keyFrames.clear();

		keyFrames.add( new TextureRegion( playerTexture, 32, 0, 16, 16 ) );
		keyFrames.add( new TextureRegion( playerTexture, 32, 16, 16, 16 ) );
		upAnim = new Animation( animationRate, keyFrames, Animation.PlayMode.LOOP );
		keyFrames.clear();

		keyFrames.add( new TextureRegion( playerTexture, 48, 0, 16, 16 ) );
		keyFrames.add( new TextureRegion( playerTexture, 48, 16, 16, 16 ) );
		rightAnim = new Animation( animationRate, keyFrames, Animation.PlayMode.LOOP );
	}

	private void setupSprite( ) {
		playerTexture = new Texture( "LinkSprites.png" );
		sprite = new Sprite( playerTexture, 0, 0, 16, 16 );
		sprite.setSize( 1, 1 );
		sprite.setPosition( (int)( Constants.WORLD_WIDTH / 2f), (int)(Constants.WORLD_HEIGHT / 2f) );
		System.out.println( sprite.getX() + ", " + sprite.getY() );
	}

	private void buildBody( World world ) {
		BodyDef bDef = new BodyDef( );
		bDef.type = BodyDef.BodyType.DynamicBody;
		bDef.position.set( sprite.getX( ) + sprite.getWidth( ) / 2.f,
				sprite.getY( ) + sprite.getHeight( ) / 2.f );

		PolygonShape shape = new PolygonShape( );
		shape.setAsBox( sprite.getWidth( ) / 2.75f, sprite.getHeight( ) / 2.75f );

		body = world.createBody( bDef );
		body.setUserData( this );

		FixtureDef fDef = new FixtureDef( );
		fDef.shape = shape;
		fDef.density = 1;
		fDef.restitution = 0;
		fDef.friction = .5f;
		body.createFixture( fDef );
		body.setFixedRotation( true );
	}

	@Override
	public void update( float delta ) {
		handleInput( delta );
		applyForce( );
		updateFacing( delta );

		sprite.setPosition(
				body.getPosition( ).x - ( sprite.getWidth( ) / 2.f ),
				body.getPosition( ).y - ( sprite.getHeight( ) / 2.f )
		);
	}

	private void updateFacing(float deltaTime){
		if(inputVelocity.len() != 0){
			currentAnimationFrame += deltaTime;

			if( inputVelocity.x < 0)
				currentFacing = Facing.LEFT;

			else if( inputVelocity.x > 0)
				currentFacing = Facing.RIGHT;

			else if( inputVelocity.y < 0)
				currentFacing = Facing.DOWN;

			else if( inputVelocity.y > 0)
				currentFacing = Facing.UP;
		}
	}

	private void applyForce( ) {
		body.setLinearVelocity( inputVelocity.x * speed, inputVelocity.y * speed );
	}

	private void handleInput( float deltaTime ) {
		inputVelocity.set( 0, 0 );

		if ( Gdx.input.isKeyPressed( Input.Keys.W ) )
			inputVelocity.y = 1;

		if ( Gdx.input.isKeyPressed( Input.Keys.A ) )
			inputVelocity.x = -1;

		if ( Gdx.input.isKeyPressed( Input.Keys.S ) )
			inputVelocity.y = -1;

		if ( Gdx.input.isKeyPressed( Input.Keys.D ) )
			inputVelocity.x = 1;
	}

	@Override
	public void draw( SpriteBatch batch ) {
		updateSprite( );
		sprite.draw( batch );
	}

	private void updateSprite( ) {
		switch(currentFacing){
			case LEFT:
				sprite.setRegion( leftAnim.getKeyFrame( currentAnimationFrame ) );
				break;
			case RIGHT:
				sprite.setRegion( rightAnim.getKeyFrame( currentAnimationFrame ) );
				break;
			case UP:
				sprite.setRegion( upAnim.getKeyFrame( currentAnimationFrame ) );
				break;
			case DOWN:
				sprite.setRegion( downAnim.getKeyFrame( currentAnimationFrame ) );
				break;
			default:
				sprite.setRegion( downAnim.getKeyFrame( currentAnimationFrame ) );
		}
	}

	public Vector2 getCenter(){
		return body.getPosition();
	}

	private enum Facing{
		LEFT, RIGHT, UP, DOWN
	}

	public void setPosition(Vector2 position){
		body.setLinearVelocity( Vector2.Zero );
		body.setTransform( position, 0 );
	}
}
