package com.redtoorange.game.desktop;

import com.badlogic.gdx.backends.lwjgl.LwjglApplication;
import com.badlogic.gdx.backends.lwjgl.LwjglApplicationConfiguration;
import com.redtoorange.game.utilities.Constants;
import com.redtoorange.game.MainGame;

public class DesktopLauncher {
	public static void main (String[] arg) {
		LwjglApplicationConfiguration config = new LwjglApplicationConfiguration();

		config.title = Constants.TITLE;

		config.width = Constants.WINDOW_WIDTH;
		config.height = Constants.WINDOW_HEIGHT;

		new LwjglApplication(new MainGame(), config);
	}
}
